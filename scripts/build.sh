#!/bin/bash

STARTTIME=$(date +%s)

# Environment
if [ -f /opt/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/opt/jdk-10.0.1
elif [ -f /usr/java/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/usr/java/jdk-10.0.1
elif [ -f '/c/Program Files/Java/jdk-10.0.1/bin/java' ]; then
export JAVA_HOME='/c/Program Files/Java/jdk-10.0.1'
else
export JAVA_HOME=`/usr/libexec/java_home -v 10`
fi

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER/desktop-buddy" ]; then
  exit 1
fi

# Build
if [ -z "$1" ]
then
DESKTOPBUDDYEPOCH=$(./epoch.sh)
else
DESKTOPBUDDYEPOCH=$1
fi

# This options are only to use at development
export MAVEN_OPTS='-XX:+TieredCompilation -XX:TieredStopAtLevel=1'

cd $PROJECTFOLDER/desktop-buddy/applications/native
./deploy
cd $PROJECTFOLDER/desktop-buddy/applications/fx
./deploy $DESKTOPBUDDYEPOCH
cd $PROJECTFOLDER/desktop-buddy/widgets/quoteoftheday
./deploy $DESKTOPBUDDYEPOCH
cd $PROJECTFOLDER/desktop-buddy/widgets/urbandictionary
./deploy $DESKTOPBUDDYEPOCH

cd $PROJECTFOLDER/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"
