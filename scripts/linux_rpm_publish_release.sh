#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export JAVA_HOME=/usr/java/jdk-10.0.1

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER"/desktop-buddy ]; then
  exit 1
fi

# Create release

if [ -z "$1" ]
then
echo Authorization token required, https://github.com/settings/tokens
exit 1
fi

RELEASENAME=v2.0.0

# Build
DESKTOPBUDDYEPOCH=$(./epoch.sh)
./linux_rpm_release.sh $DESKTOPBUDDYEPOCH

FILENAME=$PROJECTFOLDER/releases/linux/desktopbuddy-1.0-1.x86_64.rpm

# Get Latest Release ID
curl -v -i -X "GET" \
-H "Content-Type:application/json" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/repos/franferri/DesktopBuddy/releases/latest > /tmp/latest.txt
LATESTRELEASEID=$(cat /tmp/latest.txt | grep -i '"id":' | grep -m1 "" | grep -oE "[[:digit:]]{1,}") 
echo LATESTRELEASEID: $LATESTRELEASEID

echo ----------------------------------------------------------------

# Get ID of the current asset

CURRENTASSETID=$(cat /tmp/latest.txt | grep -C2 "name.:.\+$(basename $FILENAME)" | grep -m 1 "id.:" | grep -w id | tr : = | tr -cd '[[:alnum:]]=' | grep -oE "[[:digit:]]{1,}") 
echo CURRENTASSETID: $CURRENTASSETID

echo ----------------------------------------------------------------

# Delete previous file
curl -v -i -X "DELETE" \
-H "Content-Type:application/json" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/repos/franferri/DesktopBuddy/releases/assets/$CURRENTASSETID

echo ----------------------------------------------------------------

# Upload windows release
curl -v -i -X "POST" \
-H "Content-Type:application/octet-stream" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
--data-binary @"$FILENAME" \
https://uploads.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID/assets?name=$(basename $FILENAME)

cd $PROJECTFOLDER/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"
