#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export CONFIGURATIONFOLDER=~/.desktopbuddy
export OS=LNX
if [ -f /opt/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/opt/jdk-10.0.1
elif [ -f /usr/java/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/usr/java/jdk-10.0.1
elif [ -f '/c/Program Files/Java/jdk-10.0.1/bin/java' ]; then
export JAVA_HOME='/c/Program Files/Java/jdk-10.0.1'
export CONFIGURATIONFOLDER="$LOCALAPPDATA/DesktopBuddy"
export OS=WIN
else
export JAVA_HOME=`/usr/libexec/java_home -v 10`
fi

echo $CONFIGURATIONFOLDER
echo $OS
echo $JAVA_HOME

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER/test" ]; then
  exit 1
fi

# We kill running instance
./stop_test.sh

cd $PROJECTFOLDER/test

# Build
MAVEN_OPTS='-XX:+TieredCompilation -XX:TieredStopAtLevel=1'
./mvnw clean package -T 1C

ENDTIME=$(date +%s)
echo "Build took $(($ENDTIME - $STARTTIME)) seconds"

# Run
if [ -z $1 ]; then
	"$JAVA_HOME"/bin/java -jar target/test.jar Options &
	"$JAVA_HOME"/bin/java -jar target/test.jar WidgetsList &
	# "$JAVA_HOME"/bin/java -jar target/test.jar WidgetsList2 &
	"$JAVA_HOME"/bin/java -jar target/test.jar WidgetOptions &
	"$JAVA_HOME"/bin/java -jar target/test.jar Hotkeys &
	"$JAVA_HOME"/bin/java -jar target/test.jar WidgetSelector &
	# "$JAVA_HOME"/bin/java -jar target/test.jar ListViewSampleComboBox &
	# "$JAVA_HOME"/bin/java -jar target/test.jar ListViewSampleCellFactory &
	# "$JAVA_HOME"/bin/java -jar target/test.jar ListViewSampleProcessingEvents &
	# "$JAVA_HOME"/bin/java -jar target/test.jar ListViewWithCheckBox &
	# "$JAVA_HOME"/bin/java -jar target/test.jar ListViewWithStringAndCheckBox &
else
	"$JAVA_HOME"/bin/java -jar target/test.jar $1 &
fi

cd $PROJECTFOLDER/scripts
