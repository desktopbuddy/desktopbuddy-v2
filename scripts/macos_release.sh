#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export JAVA_HOME=`/usr/libexec/java_home -v 10`

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER"/desktop-buddy ]; then
  exit 1
fi

# Build
if [ -z "$1" ]
then
DESKTOPBUDDYEPOCH=$(./epoch.sh)
else
DESKTOPBUDDYEPOCH=$1
fi

./build.sh $DESKTOPBUDDYEPOCH

# Release
rm -rf "$PROJECTFOLDER"/releases/macos
mkdir -p "$PROJECTFOLDER"/releases/macos
rm -rf "$PROJECTFOLDER"/releases/updates
mkdir -p "$PROJECTFOLDER"/releases/updates

if [ ! -d "$PROJECTFOLDER"/releases/macos ]; then
  exit 1
fi

# Java packager - TO IMPROVE WITH: https://docs.oracle.com/javase/10/tools/javapackager.htm
"$JAVA_HOME"/bin/javapackager -deploy -Bruntime="$JAVA_HOME" \
 -native image \
 -outdir "$PROJECTFOLDER"/releases/macos \
 -outfile DesktopBuddy.app \
 -srcdir "$PROJECTFOLDER"/desktop-buddy/applications/native/target \
 -srcfiles desktopbuddy.jar \
 -appclass desktopbuddy.Main \
 -name "DesktopBuddy" \
 -title "DesktopBuddy" \
 -Bicon="$PROJECTFOLDER/artwork/icons/Fried Egg by Mickka/macos.icns"

sed -i.backup 's/<string>APPL<\/string>/<string>APPL<\/string>\
  <key>LSUIElement<\/key>\
  <string>1<\/string>/' "$PROJECTFOLDER"/releases/macos/DesktopBuddy.app/Contents/Info.plist

rm "$PROJECTFOLDER"/releases/macos/DesktopBuddy.app/Contents/Info.plist.backup

# Compress
echo Compressing...
cd "$PROJECTFOLDER"/releases/macos
zip -r "$PROJECTFOLDER"/releases/macos/macos-desktopbuddy.zip *.app
rm -rf "$PROJECTFOLDER"/releases/macos/*.app

JARFILENAME=runtime_$DESKTOPBUDDYEPOCH.jar
ZIPFILENAME=updates_runtime_$DESKTOPBUDDYEPOCH.zip
mv "$PROJECTFOLDER"/desktop-buddy/applications/fx/target/$JARFILENAME "$PROJECTFOLDER"/desktop-buddy/applications/fx/target/new.$JARFILENAME
zip -r -j "$PROJECTFOLDER"/releases/updates/$ZIPFILENAME "$PROJECTFOLDER"/desktop-buddy/applications/fx/target/new.$JARFILENAME

JARFILENAME=widget_quoteoftheday_$DESKTOPBUDDYEPOCH.jar
ZIPFILENAME=updates_widget_quoteoftheday_$DESKTOPBUDDYEPOCH.zip
mv "$PROJECTFOLDER"/desktop-buddy/widgets/quoteoftheday/target/$JARFILENAME "$PROJECTFOLDER"/desktop-buddy/widgets/quoteoftheday/target/new.$JARFILENAME
zip -r -j "$PROJECTFOLDER"/releases/updates/$ZIPFILENAME "$PROJECTFOLDER"/desktop-buddy/widgets/quoteoftheday/target/new.$JARFILENAME

JARFILENAME=widget_urbandictionary_$DESKTOPBUDDYEPOCH.jar
ZIPFILENAME=updates_widget_urbandictionary_$DESKTOPBUDDYEPOCH.zip
mv "$PROJECTFOLDER"/desktop-buddy/widgets/urbandictionary/target/$JARFILENAME "$PROJECTFOLDER"/desktop-buddy/widgets/urbandictionary/target/new.$JARFILENAME
zip -r -j "$PROJECTFOLDER"/releases/updates/$ZIPFILENAME "$PROJECTFOLDER"/desktop-buddy/widgets/urbandictionary/target/new.$JARFILENAME

cd "$PROJECTFOLDER"/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"