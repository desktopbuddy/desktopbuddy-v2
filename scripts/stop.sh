#!/bin/bash

# Environment
export CONFIGURATIONFOLDER=~/.desktopbuddy
export OS=LNX
if [ -f /opt/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/opt/jdk-10.0.1
elif [ -f /usr/java/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/usr/java/jdk-10.0.1
elif [ -f '/c/Program Files/Java/jdk-10.0.1/bin/java' ]; then
export JAVA_HOME='/c/Program Files/Java/jdk-10.0.1'
export CONFIGURATIONFOLDER="$LOCALAPPDATA/DesktopBuddy"
export OS=WIN
else
export JAVA_HOME=`/usr/libexec/java_home -v 10`
fi

if [ $OS = "LNX" ]; then
pkill -9 -f desktopbuddy
pkill -9 -f DesktopBuddy
else
cmd "/C taskkill /F /IM "java*" /T"
cmd "/C taskkill /F /IM "desktopbuddy.exe" /T"
fi