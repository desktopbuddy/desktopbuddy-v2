#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export JAVA_HOME="/c/Program Files/Java/jdk-10.0.1"

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER"/desktop-buddy ]; then
  exit 1
fi

# Build
if [ -z "$1" ]
then
DESKTOPBUDDYEPOCH=$(./epoch.sh)
else
DESKTOPBUDDYEPOCH=$1
fi

./build.sh $DESKTOPBUDDYEPOCH

# Release
rm -rf "$PROJECTFOLDER"/releases/windows
mkdir -p "$PROJECTFOLDER"/releases/windows

if [ ! -d "$PROJECTFOLDER"/releases/windows ]; then
  exit 1
fi

# Java packager - TO IMPROVE WITH: https://docs.oracle.com/javase/10/tools/javapackager.htm
mkdir -p "$PROJECTFOLDER"/releases/windows/build

cp "$PROJECTFOLDER"/license "$PROJECTFOLDER"/releases/windows/build
cp "$PROJECTFOLDER"/desktop-buddy/applications/native/target/desktopbuddy.jar "$PROJECTFOLDER"/releases/windows/build

"$JAVA_HOME"/bin/javapackager -deploy \
 -native exe \
 -Bruntime="$JAVA_HOME" \
 -BsystemWide=true \
 -outdir "$PROJECTFOLDER"/releases/windows \
 -srcdir "$PROJECTFOLDER"/releases/windows/build \
 -appclass desktopbuddy.Main \
 -name "DesktopBuddy" \
 -title "Desktop Buddy" \
 -Bicon="$PROJECTFOLDER/artwork/icons/Fried Egg by Mickka/windows.ico"

rm -rf "$PROJECTFOLDER"/releases/windows/build

cd "$PROJECTFOLDER"/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"