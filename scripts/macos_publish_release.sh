#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export JAVA_HOME=`/usr/libexec/java_home -v 10`

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER/desktop-buddy" ]; then
  exit 1
fi

# Create release

if [ -z "$1" ]
then
echo Authorization token required, https://github.com/settings/tokens
exit 1
fi

RELEASENAME=v2.0.0

# Build
DESKTOPBUDDYEPOCH=$(./epoch.sh)
./macos_release.sh $DESKTOPBUDDYEPOCH

# Get Latest Release ID
curl -v -i -X "GET" \
-H "Content-Type:application/json" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/repos/franferri/DesktopBuddy/releases/latest > /tmp/latest.txt
LATESTRELEASEID=$(cat /tmp/latest.txt | grep -i '"id":' | grep -m1 "" | grep -oE "[[:digit:]]{1,}") 
echo $LATESTRELEASEID

echo ----------------------------------------------------------------

# Delete Draft Release
curl -v -i -X "DELETE" \
-H "Content-Type:application/json" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID

echo ----------------------------------------------------------------

# Delete local tags.
git tag -d $(git tag -l)
# Fetch remote tags.
git fetch
# Delete remote tags.
git push origin --delete $(git tag -l) # Pushing once should be faster than multiple times
# Delete local tags.
git tag -d $(git tag -l)

echo ----------------------------------------------------------------

# Create Release
curl -v -i -X "POST" \
-H "Content-Type:application/json" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
https://api.github.com/repos/franferri/DesktopBuddy/releases \
-d '{"tag_name":"v2.0.0","target_commitish": "master","name": "v2.0.0","body": "","draft": false,"prerelease": false}' > /tmp/latest.txt

echo ----------------------------------------------------------------

# Get New Release ID
LATESTRELEASEID=$(cat /tmp/latest.txt | grep -i '"id":' | grep -m1 "" | grep -oE "[[:digit:]]{1,}") 
echo $LATESTRELEASEID

echo ----------------------------------------------------------------

# Upload macOS release
FILENAME=$PROJECTFOLDER/releases/macos/macos-desktopbuddy.zip
curl -v -i -X "POST" \
-H "Content-Type:application/octet-stream" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
--data-binary @"$FILENAME" \
https://uploads.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID/assets?name=$(basename $FILENAME)

# Upload runtime
FILENAME=$PROJECTFOLDER/releases/updates/updates_runtime_$DESKTOPBUDDYEPOCH.zip
curl -v -i -X "POST" \
-H "Content-Type:application/octet-stream" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
--data-binary @"$FILENAME" \
https://uploads.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID/assets?name=$(basename $FILENAME)

# Upload widgets
FILENAME=$PROJECTFOLDER/releases/updates/updates_widget_urbandictionary_$DESKTOPBUDDYEPOCH.zip
curl -v -i -X "POST" \
-H "Content-Type:application/octet-stream" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
--data-binary @"$FILENAME" \
https://uploads.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID/assets?name=$(basename $FILENAME)

FILENAME=$PROJECTFOLDER/releases/updates/updates_widget_quoteoftheday_$DESKTOPBUDDYEPOCH.zip
curl -v -i -X "POST" \
-H "Content-Type:application/octet-stream" \
-H "Authorization: token $1" \
-H "Accept: application/vnd.github.v3+json" \
--data-binary @"$FILENAME" \
https://uploads.github.com/repos/franferri/DesktopBuddy/releases/$LATESTRELEASEID/assets?name=$(basename $FILENAME)


cd $PROJECTFOLDER/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"