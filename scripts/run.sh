#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export CONFIGURATIONFOLDER=~/.desktopbuddy
export OS=LNX
if [ -f /opt/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/opt/jdk-10.0.1
elif [ -f /usr/java/jdk-10.0.1/bin/java ]; then
export JAVA_HOME=/usr/java/jdk-10.0.1
elif [ -f '/c/Program Files/Java/jdk-10.0.1/bin/java' ]; then
export JAVA_HOME='/c/Program Files/Java/jdk-10.0.1'
export CONFIGURATIONFOLDER="$LOCALAPPDATA/DesktopBuddy"
export OS=WIN
else
export JAVA_HOME=`/usr/libexec/java_home -v 10`
fi

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER/desktop-buddy" ]; then
  exit 1
fi

# We kill running instance
./stop.sh

# Removing conf from previous installations
rm -rf "$CONFIGURATIONFOLDER"

# Build
DESKTOPBUDDYEPOCH=$(./epoch.sh)
./build.sh $DESKTOPBUDDYEPOCH

# Prepare to run
mkdir -p "$CONFIGURATIONFOLDER/runtime"
mkdir -p "$CONFIGURATIONFOLDER/widgets"

cp "$PROJECTFOLDER/desktop-buddy/applications/fx/target/runtime_$DESKTOPBUDDYEPOCH.jar" "$CONFIGURATIONFOLDER/runtime/"
cp "$PROJECTFOLDER/desktop-buddy/widgets/quoteoftheday/target/widget_quoteoftheday_$DESKTOPBUDDYEPOCH.jar" "$CONFIGURATIONFOLDER/widgets/"
cp "$PROJECTFOLDER/desktop-buddy/widgets/urbandictionary/target/widget_urbandictionary_$DESKTOPBUDDYEPOCH.jar" "$CONFIGURATIONFOLDER/widgets/"

# Run
cd "$PROJECTFOLDER/desktop-buddy/applications/native"
"$JAVA_HOME"/bin/java -jar target/desktopbuddy.jar

cd "$PROJECTFOLDER/scripts"

ENDTIME=$(date +%s)
echo "Execution lasted $(($ENDTIME - $STARTTIME)) seconds"
