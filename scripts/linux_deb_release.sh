#!/bin/bash

STARTTIME=$(date +%s)

# Environment
export JAVA_HOME=/opt/jdk-10.0.1

PROJECTFOLDER="$( cd "$(dirname "$0")" ; pwd -P )"/..
if [ ! -d "$PROJECTFOLDER"/desktop-buddy ]; then
  exit 1
fi

# Build
if [ -z "$1" ]
then
DESKTOPBUDDYEPOCH=$(./epoch.sh)
else
DESKTOPBUDDYEPOCH=$1
fi

./build.sh $DESKTOPBUDDYEPOCH

# Release
rm -rf "$PROJECTFOLDER"/releases/linux
mkdir -p "$PROJECTFOLDER"/releases/linux

if [ ! -d "$PROJECTFOLDER"/releases/linux ]; then
  exit 1
fi

# Java packager - TO IMPROVE WITH: https://docs.oracle.com/javase/10/tools/javapackager.htm
mkdir -p "$PROJECTFOLDER"/releases/linux/build

cp "$PROJECTFOLDER"/license "$PROJECTFOLDER"/releases/linux/build
cp "$PROJECTFOLDER"/desktop-buddy/applications/native/target/desktopbuddy.jar "$PROJECTFOLDER"/releases/linux/build

"$JAVA_HOME"/bin/javapackager -deploy \
 -Bruntime="$JAVA_HOME" \
 -outdir "$PROJECTFOLDER"/releases/linux \
 -srcdir "$PROJECTFOLDER"/releases/linux/build \
 -appclass desktopbuddy.Main \
 -name "DesktopBuddy" \
 -title "Desktop Buddy" \
 -Bicon="$PROJECTFOLDER/artwork/icons/Fried Egg by Mickka/linux.png" \
 -native deb \
 -BlicenseFile=license \
 -BlicenseType=GPLv3

rm -rf "$PROJECTFOLDER"/releases/linux/build

cd "$PROJECTFOLDER"/scripts

ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds"

