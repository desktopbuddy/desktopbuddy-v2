## Desktop Buddy

### Regular user installers

| macOS                                                                                                           | Windows                                                                                                  | Linux deb                                                                                                | Linux rpm                                                                                                |
|-----------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| [desktopbuddy.zip<sup>*</sup>](https://github.com/franferri/desktop-buddy/releases/download/v2.0.0/macos-desktopbuddy.zip) | [desktopbuddy.exe](https://github.com/franferri/desktop-buddy/releases/download/v2.0.0/desktopbuddy.exe) | [desktopbuddy.deb](https://github.com/franferri/desktop-buddy/releases/download/v2.0.0/desktopbuddy.deb) | [desktopbuddy.rpm](https://github.com/franferri/desktop-buddy/releases/download/v2.0.0/desktopbuddy.rpm) |

<sup>*</sup> macOS will ask for for permission the first time you run it if you enable capture key capability.

DesktopBuddy will create a ~/.desktopbuddy folder in linux amd macOS or %APPDATA%/desktopbuddy folder in windows for configurations.

You can always go straight to the [releases page](https://github.com/franferri/desktop-buddy/releases/latest) and pick your own

### Developers

These are just a bunch of maven projects

    git clone https://github.com/franferri/desktop-buddy.git    
    cd desktop-buddy/scripts/run.sh

In windows you'll have to use Git Bash console to run the commands


