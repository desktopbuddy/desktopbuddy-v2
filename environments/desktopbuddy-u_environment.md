Environment desktopbuddy-u

Ubuntu 18.04 Desktop

# vmware

HD 20GB -> not splitted, 1 File
CPU x2
RAM 2048
No graphics acceleration, No usb, no webcam, no printer
Network -> Bridged
Isolation -> No drag and drop

# .vmx file

MemTrimRate = "0"
mc.version = "0"
sched.mem.pshare.enable = "FALSE"
mainMem.useNamedFile = "FALSE"
prefvmx.minVmMemPct = "100"
prefvmx.useRecommendedLockedMemSize = "TRUE"
mainMem.partialLazySave = "FALSE"
mainMem.partialLazyRestore = "FALSE"
priority.grabbed = "high"
priority.ungrabbed = "normal"

# installation

Language: English
Keyboard: Spanish / Spanish
Minimal installation
Download updates while install
Erase disk and install ubuntu
Location / Timezone: Dublin
LogIn automatically

# Display, system updates and power options

1280x800
Auto updater till the end
Settings -> Power -> Blank Screen never
Settings -> Privacy -> Screen lock off

# Disable Automatic Updates
	sudo nano /etc/apt/apt.conf.d/20auto-upgrades
	APT::Periodic::Update-Package-Lists "1"; -> "0";

Stop the machine and remove the cdrom

# vmware tools and git

	sudo apt-get install open-vm-tools open-vm-tools-desktop git fakeroot curl -y
	# reboot
	git config --global user.name "Fran Ferri"
	git config --global user.email "franferri@gmail.com"

# Java
	cd /opt

http://www.oracle.com/technetwork/java/javase/overview/index.html

	sudo wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/10.0.1+10/fb4372174a714e6b8c52526dc134031e/jdk-10.0.1_linux-x64_bin.tar.gz
	
	sudo tar xvzf jdk-10.0.1_linux-x64_bin.tar.gz
	sudo rm -f jdk-10.0.1_linux-x64_bin.tar.gz
	/opt/jdk-10.0.1/bin/java -version


# DesktopBuddy
	cd ~
	rm -rf ~/.m2
	rm -rf ~/.desktopbuddy
	rm -rf ~/desktop-buddy
	git clone https://github.com/franferri/desktop-buddy.git
	# git config --local user.name "Fran Ferri"
	# git config --local user.email "franferri@gmail.com"
	cd desktop-buddy
	git checkout v2
	cd ~/desktop-buddy/scripts
	./run.sh




