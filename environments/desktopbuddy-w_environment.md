Environment desktopbuddy-w

Windows 10

# vmware

HD 60GB -> not splitted, 1 File
CPU x2
RAM 4096
Default Applications -> remove open your mac files
No graphics acceleration, No usb, no webcam, no printer
Network -> Bridged
Isolation -> No drag and drop
Legacy BIOS

# .vmx file

MemTrimRate = "0"
mc.version = "0"
sched.mem.pshare.enable = "FALSE"
mainMem.useNamedFile = "FALSE"
prefvmx.minVmMemPct = "100"
prefvmx.useRecommendedLockedMemSize = "TRUE"
mainMem.partialLazySave = "FALSE"
mainMem.partialLazyRestore = "FALSE"
priority.grabbed = "high"
priority.ungrabbed = "normal"

# installation

Region: Ireland
Keyboard layout: spanish
Personal use
Offline account
Disable all location, diagnostic, etc... options

LogIn automatically -> netplwiz -> uncheck "users must enter a user name and password to use this computer"

# vmware tools

Install from fusion, DVD unit needed
After installation remove the cdrom

# Display, system updates and power options

1280x800
Control panel -> power options -> chose when to turn off the display -> never, never
System Properties -> advanced -> performance -> settings -> no page file
services.msc -> windows update -> disable

# Machine name
desktopbudy-w

## Git
	https://git-scm.com/download/win
	File encoding "as it is"

	git config --global user.name "Fran Ferri"
	git config --global user.email "franferri@gmail.com"

# Java
	http://www.oracle.com/technetwork/java/javase/overview/index.html

# Inno setup
Download the unicode version: http://www.jrsoftware.org/download.php/is-unicode.exe
Control Panel -> Show small icons -> System -> Advanced system settings -> Edit environmental variables -> System variables -> Path -> we add C:\Program Files (x86)\Inno Setup 5


# clean.bat (run as administrator)
powercfg -h off
dism.exe /Online /Cleanup-image /Restorehealth
%windir%\system32\cleanmgr.exe

# DesktopBuddy -> Git Bash terminal
	cd ~
	rm -rf ~/.m2
	rm -rf ~/.desktopbuddy
	rm -rf ~/desktop-buddy
	git clone https://github.com/franferri/desktop-buddy.git
	# git config --local user.name "Fran Ferri"
	# git config --local user.email "franferri@gmail.com"
	cd desktop-buddy
	git checkout v2
	cd ~/desktop-buddy/scripts
	./run.sh



