Environment desktopbuddy-m

macOS 10.13

# vmware

We add external harddrive from another unit to be faster,
Editing the .vmx file and changing
	sata0:2.fileName = "Temporary Installation Source Disk.vmdk"
for
	sata0:2.fileName = "Temporary Installation Source Disk.vmdk"
	fileSearchPath = "/Volumes/software_distribution/operating_systems/macOS 10.13;."

HD 40GB -> not splitted, 1 File
CPU x2
RAM 2048
No graphics acceleration, no webcam, no printer
Network -> Bridged
Isolation -> No drag and drop
UEFI BIOS

# .vmx file

MemTrimRate = "0"
mc.version = "0"
sched.mem.pshare.enable = "FALSE"
mainMem.useNamedFile = "FALSE"
prefvmx.minVmMemPct = "100"
prefvmx.useRecommendedLockedMemSize = "TRUE"
mainMem.partialLazySave = "FALSE"
mainMem.partialLazyRestore = "FALSE"
priority.grabbed = "high"
priority.ungrabbed = "normal"
smc.version = "0"

# installation

Language: English
Disk utility -> Machintosh HD -> Erase -> MacOS Extended (Journaled)
Install macOS
Keyboard Icon (top right screen) -> Spanish ISO
Region: Ireland
Keyboard: Spanish ISO
Do not transfer from other computers
apple id: Setup later
Locatoin Services: Disabled
TimeZone: Dublin Ireland
LogIn automatically: System settings -> users -> login options -> automatic login
Shutdown
Remove the additional installation hard drive in vmware machine settings

# Disable Automatic Updates
App Store -> barra de menu -> App Store -> Preferences -> uncheck Download newly available updates in the background.

Terminal -> Preferences -> Profiles -> Shell -> close terminal if the process ends well

# Spotlight indexing
Disable all but keep Application

# vmware tools
You need the dvdrom pressent, install from fusion or workstation
After rebooting you must remove again any screen acceleration or full retina configuration
Remove the DVDrom
Keyboard profile en vmware to Default
Text editor, preferences -> plain text
System Preferences -> Users -> Login Items -> Remove all

# Machine name
System Preferences -> Shared -> desktopbudy-m

# Display

1280x800

# change_resolution.command
	#!/bin/bash
	/Library/Application\ Support/VMware\ Tools/vmware-resolutionSet 1280 800

	System Preferences -> Users -> Login Items -> Add change_resolution.command

## Git
	xcode-select --install

	git config --global user.name "Fran Ferri"
	git config --global user.email "franferri@gmail.com"


# Java

	cd ~/Desktop
	http://www.oracle.com/technetwork/java/javase/overview/index.html

	curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O http://download.oracle.com/otn-pub/java/jdk/10.0.1+10/fb4372174a714e6b8c52526dc134031e/jdk-10.0.1_osx-x64_bin.dmg

	Borramos el fichero .dmg
	/usr/libexec/java_home -v 10
	/Library/Java/JavaVirtualMachines/jdk-10.0.1.jdk/Contents/Home/bin/java -version

# DesktopBuddy
	cd ~
	rm -rf ~/.m2
	rm -rf ~/.desktopbuddy
	rm -rf ~/desktop-buddy
	git clone https://github.com/franferri/desktop-buddy.git
	# git config --local user.name "Fran Ferri"
	# git config --local user.email "franferri@gmail.com"
	cd desktop-buddy
	git checkout v2
	cd ~/desktop-buddy/scripts
	./run.sh

