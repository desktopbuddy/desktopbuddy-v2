Environment desktopbuddy-c

Centos 7 Desktop

# vmware

HD 20GB -> not splitted, 1 File
CPU x2
RAM 2048
No graphics acceleration, No usb, no webcam, no printer
Network -> Bridged
Isolation -> No drag and drop

# .vmx file

MemTrimRate = "0"
mc.version = "0"
sched.mem.pshare.enable = "FALSE"
mainMem.useNamedFile = "FALSE"
prefvmx.minVmMemPct = "100"
prefvmx.useRecommendedLockedMemSize = "TRUE"
mainMem.partialLazySave = "FALSE"
mainMem.partialLazyRestore = "FALSE"
priority.grabbed = "high"
priority.ungrabbed = "normal"

# installation

Language: English / English (United States)
Location / Timezone: Dublin
Keyboard: Spanish / Spanish
Software Selection: Gnome Desktop
Erase disk and install linux mint
Enable etheret
Make user administrator

# Display, system updates and power options

1280x800
Settings -> Power -> Blank Screen never
Settings -> Privacy -> Screen lock off
Auto updater till the end
Settings -> Users -> Automatic log in

# Disable Automatic Updates
systemctl stop packagekit
systemctl mask packagekit

Stop the machine and remove the cdrom

# vmware tools

	sudo yum install open-vm-tools open-vm-tools-desktop git rpm-build -y
	# reboot
	git config --global user.name "Fran Ferri"
	git config --global user.email "franferri@gmail.com"

# Java
	cd /opt

http://www.oracle.com/technetwork/java/javase/overview/index.html

	sudo wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/10.0.1+10/fb4372174a714e6b8c52526dc134031e/jdk-10.0.1_linux-x64_bin.rpm
	sudo yum localinstall jdk-10.0.1_linux-x64_bin.rpm -y
	sudo rm -f jdk-10.0.1_linux-x64_bin.rpm
	/usr/java/jdk-10.0.1/bin/java -version


# DesktopBuddy
	cd ~
	rm -rf ~/.m2
	rm -rf ~/.desktopbuddy
	rm -rf ~/desktop-buddy
	git clone https://github.com/franferri/desktop-buddy.git
	# git config --local user.name "Fran Ferri"
	# git config --local user.email "franferri@gmail.com"
	cd desktop-buddy
	git checkout v2
	cd ~/desktop-buddy/scripts
	./run.sh


