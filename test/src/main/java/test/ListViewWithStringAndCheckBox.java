package test;

import helpers.Randomizer;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ListViewWithStringAndCheckBox extends Application {

	@Override
	public void start(Stage stage) {

		// We add the Items to the listbox
		ListView<String> listView = new ListView<>();
		for (int i = 1; i <= 20; i++) {
			String item = "Item " + i;
			listView.getItems().add(item);
		}

		// We detect the click for the checkboxes
		Callback<String, ObservableValue<Boolean>> callback = new Callback<String, ObservableValue<Boolean>>() {
			@Override
			public ObservableValue<Boolean> call(String item) {

				BooleanProperty observable = new SimpleBooleanProperty();

				observable.addListener((obs, wasSelected, isNowSelected) -> System.out.println("Check box for " + item + " changed from " + wasSelected + " to " + isNowSelected));

				return observable;
			}
		};

		// We add the checkboxes to the Items
		Callback<ListView<String>, ListCell<String>> checkBoxListCell = CheckBoxListCell.forListView(callback);
		listView.setCellFactory(checkBoxListCell);

		BorderPane root = new BorderPane(listView);
		Scene scene = new Scene(root, 250, 400);
		Widget.normalize(stage, scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}