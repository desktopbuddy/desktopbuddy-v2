package test;

import helpers.Randomizer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Widget extends Application {

	private static String name;

	public static void setWidgetName(String widgetName) {
		name = widgetName;
	}

	@Override
	public void start(Stage stage) throws Exception {

		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(name + ".fxml"));
		Scene scene = new Scene(root);
		normalize(stage, scene);
		stage.show();
	}

	public static void normalize(Stage stage, Scene scene) {
		scene.getStylesheets().add(Widget.class.getClassLoader().getResource("crossplatform.css").toExternalForm());
		stage.setScene(scene);
		Randomizer.randomLocation(stage, scene);
	}

}