package test;

import helpers.Randomizer;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ListViewWithCheckBox extends Application {

	@Override
	public void start(Stage stage) {

		ListView<Item> listView = new ListView<>();

		for (int i = 1; i <= 20; i++) {
			Item item = new Item("Item " + i, false);

			item.onProperty().addListener((obs, wasOn, isNowOn) -> {
				System.out.println(item.name() + " changed on state from " + wasOn + " to " + isNowOn);
			});

			listView.getItems().add(item);
		}

		Callback<ListView<Item>, ListCell<Item>> checkBoxListCell = CheckBoxListCell.forListView((item) -> {
			return item.onProperty();
		});

		listView.setCellFactory(checkBoxListCell);

		BorderPane root = new BorderPane(listView);
		Scene scene = new Scene(root, 250, 400);
		Widget.normalize(stage, scene);
		stage.show();

	}

	public static class Item {

		private final StringProperty name = new SimpleStringProperty();
		private final BooleanProperty on = new SimpleBooleanProperty();

		public Item(String name, boolean on) {
			setName(name);
			setOn(on);
		}

		private final StringProperty nameProperty() {
			return this.name;
		}

		private final BooleanProperty onProperty() {
			return this.on;
		}

		public final String name() {
			return this.nameProperty().get();
		}

		public final void setName(final String name) {
			this.nameProperty().set(name);
		}

		public final boolean isOn() {
			return this.onProperty().get();
		}

		public final void setOn(final boolean on) {
			this.onProperty().set(on);
		}

		@Override
		public String toString() {
			return name();
		}

	}

}