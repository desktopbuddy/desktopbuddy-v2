package helpers;

import java.util.Random;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Randomizer {

	public static void randomLocation(Stage stage, Scene scene) {
		int padding = 50;
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		Random random = new Random();

		Double maxWidth = primaryScreenBounds.getWidth() - scene.getWidth() - padding;
		Double maxHeight = primaryScreenBounds.getHeight() - scene.getHeight() - padding;

		int width = random.nextInt(maxWidth.intValue() - padding + 1) + padding;
		int height = random.nextInt(maxHeight.intValue() - padding + 1) + padding;

		stage.setX(width);
		stage.setY(height);
	}

}
