import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import test.Widget;

public class Main {

	public static final Logger log = LogManager.getLogger(Main.class);

	public static void main(String[] args) {

		Class<? extends Application> test;

		try {

			try {
				test = (Class<? extends Application>) Class.forName("test." + args[0]);
			} catch (Exception e) {
				test = Widget.class;
				Widget.setWidgetName(args[0]);
			}

			Application.launch(test);

		} catch (Exception e) {
			log.error("Error running test: ", e);
		}
	}

}
