package desktopbuddy.model;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Jar {

	public static final Logger log = LogManager.getLogger(Jar.class);

	private SimpleJar newJar;
	private SimpleJar currentJar;

	String name;

	public Jar(SimpleJar newJar, SimpleJar currentJar, String name) {

		this.newJar = newJar;
		this.currentJar = currentJar;

		this.name = name;

	}

	public File newJar() {
		return (null == newJar ? new File("") : newJar.file());
	}

	public Long newJarVersion() {
		return (null == newJar ? 0l : newJar.version());
	}

	public File currentJar() {
		return (null == currentJar ? new File("") : currentJar.file());
	}

	public Long currentJarVersion() {
		return (null == currentJar ? 0l : currentJar.version());
	}

	public String name() {
		return name;
	}

}
