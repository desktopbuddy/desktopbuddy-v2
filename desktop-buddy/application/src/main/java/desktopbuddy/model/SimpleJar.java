package desktopbuddy.model;

import java.io.File;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SimpleJar {

	public static final Logger log = LogManager.getLogger(SimpleJar.class);

	private File file;
	private Long version;

	private SimpleJar(File file, Long version) {

		log.debug("Detected file: " + file.getAbsolutePath());

		this.file = file;
		this.version = version;

	}

	public static SimpleJar loadJars(String path, String prefix, String name, boolean delete) {

		final String suffix = ".jar";

		SimpleJar _file = null;

		File[] files = new File(path).listFiles((dir, fileName) -> {
			String filePrefix = prefix + (name != null && !name.isEmpty() ? "_" + name + "_" : "");
			return (fileName.startsWith(filePrefix) && fileName.endsWith(suffix));
		});

		if (null != files) {

			Long version;
			for (File file : files) {

				String fileName = file.getName();
				version = Long.valueOf(fileName.substring(prefix.length() + 1 + (name != null && !name.isEmpty() ? name.length() + 1 : 0), fileName.length() - suffix.length()));

				if (null == _file) {
					_file = new SimpleJar(file, version);
				}

				if (version > _file.version()) { // We just keep the newest version
					_file = new SimpleJar(file, version);
				}

			}

			if (delete) {
				for (File file : files) {
					if (Objects.equals(file.getName(), _file.file().getName())) {
						continue;
					}
					file.delete();
				}
			}

		}

		return _file;

	}

	public File file() {
		return file;
	}

	public Long version() {
		return version;
	}

}
