package desktopbuddy.helpers;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Reflect {

	public static final Logger log = LogManager.getLogger(Reflect.class);

	// https://www.google.ie/search?q=java+load+jar+dynamically&oq=java+load+jar+&aqs=chrome.1.69i57j69i59j0l4.5148j0j9&sourceid=chrome&ie=UTF-8
	// https://stackoverflow.com/questions/60764/how-should-i-load-jars-dynamically-at-runtime

	// Load and instantiate a class: http://stackabuse.com/example-loading-a-java-class-at-runtime/

	public static Class<?> loadClass(File jar, String classpath, ClassLoader classLoader) throws Exception {
		ClassLoader loader = URLClassLoader.newInstance(new URL[] { jar.toURI().toURL() }, classLoader);
		return Class.forName(classpath, true, loader);
	}

	public static <E> Class<? extends E> loadClassCheckingType(File jar, String classpath, Class<E> typeToCheckCasting, ClassLoader classLoader) throws Exception {
		ClassLoader loader = URLClassLoader.newInstance(new URL[] { jar.toURI().toURL() }, classLoader);
		Class<?> clazz = Class.forName(classpath, true, loader);
		Class<? extends E> castedClass = clazz.asSubclass(typeToCheckCasting);
		return castedClass;
	}

	public static <E> List<E> loadClassCheckingTypeAndInstantiate(String directory, String classpath, Class<E> typeToCheckCasting, ClassLoader classLoader) throws Exception {
		List<E> objects = new ArrayList<>();
		File jarsDir = new File(directory);
		for (File jar : jarsDir.listFiles()) {
			try {
				Class<? extends E> castedClass = loadClassCheckingType(jar, classpath, typeToCheckCasting, classLoader);
				Constructor<? extends E> constructor = castedClass.getDeclaredConstructor();
				objects.add(constructor.newInstance());
			} catch (ClassNotFoundException e) {
				continue;
			}
		}
		return objects;
	}

	/**
	 * Reflect.callStaticMethod("desktopbuddy.Main", "ping", null, null, DesktopBuddy.class.getClassLoader());
	 **/
	public static Object callStaticMethod(String classpath, String methodName, Class<?> parameterTypes[], Object parameters[], ClassLoader classLoader) throws Exception {
		Class<?> classWithStaticMethod = Class.forName(classpath, true, classLoader);
		Method staticMethod = classWithStaticMethod.getDeclaredMethod(methodName, parameterTypes);
		staticMethod.setAccessible(true); // override protected or private
		return staticMethod.invoke(null, parameters);
	}

}
