package desktopbuddy.helpers.configuration;

public class Widget {

	public String name;

	public String sourceURL;

	public Integer[] hotkeys;
	public Integer hotkeysTimes;

	private Object name() {
		return name;
	}

	public String getName() {
		return name;
	}

	public String getSourceURL() {
		return sourceURL;
	}

	public Integer[] getHotkeys() {
		return hotkeys;
	}

	public Integer getHotkeysTimes() {
		return hotkeysTimes;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("name");
		sb.append(": ");
		sb.append(name());

		return sb.toString();
	}

}
