package desktopbuddy.helpers.configuration;

import java.util.ArrayList;
import java.util.List;

public class Config {

	private boolean autoUpdate = true;

	// private String releasesSourceURL; // Where to get updates of runtime (and official widgets?)

	private boolean autoStartOnBoot = true;

	private boolean enableKeybardDetection = true;
	private boolean keyboardDetectionPerWidget = false;

	private Integer[] globalHotkeys;
	private Integer globalHotkeysTimes;

	private List<Widget> widgets = new ArrayList<>();

	public boolean isAutoUpdateEnabled() {
		return autoUpdate;
	}

	public void setAutoUpdate(boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}

	// public String releasesSourceURL() {
	// return releasesSourceURL;
	// }

	// public void setReleasesSourceURL(String releasesSourceURL) {
	// this.releasesSourceURL = releasesSourceURL;
	// }

	public boolean isAutoStartOnBoot() {
		return autoStartOnBoot;
	}

	public void setAutoStartOnBoot(boolean autoStartOnBoot) {
		this.autoStartOnBoot = autoStartOnBoot;
	}

	public boolean isEnableKeybardDetection() {
		return enableKeybardDetection;
	}

	public void setEnableKeybardDetection(boolean enableKeybardDetection) {
		this.enableKeybardDetection = enableKeybardDetection;
	}

	public boolean isKeyboardDetectionPerWidget() {
		return keyboardDetectionPerWidget;
	}

	public void setKeyboardDetectionPerWidget(boolean keyboardDetectionPerWidget) {
		this.keyboardDetectionPerWidget = keyboardDetectionPerWidget;
	}

	public Integer[] globalHotkeys() {
		return globalHotkeys;
	}

	public void setGlobalHotkeys(Integer[] globalHotkeys) {
		this.globalHotkeys = globalHotkeys;
	}

	public Integer globalHotkeysTimes() {
		return globalHotkeysTimes;
	}

	public void setGlobalHotkeysTimes(Integer globalHotkeysTimes) {
		this.globalHotkeysTimes = globalHotkeysTimes;
	}

	public List<Widget> widgets() {
		return widgets;
	}

	public void setWidgets(List<Widget> widgets) {
		this.widgets = widgets;
	}

	public Widget widget(String name) {
		for (Widget widget : widgets) {
			if (widget.name.equals(name)) {
				return widget;
			}
		}
		return null;
	}

	public void newWidget(String name) {
		Widget newWidget = new Widget();
		newWidget.name = name;
		widgets.add(newWidget);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("autoUpdate");
		sb.append(": ");
		sb.append(isAutoUpdateEnabled());
		sb.append(", ");
		sb.append("autoStartOnBoot");
		sb.append(": ");
		sb.append(isAutoStartOnBoot());
		sb.append(", ");
		sb.append("enableKeybardDetection");
		sb.append(": ");
		sb.append(isEnableKeybardDetection());
		sb.append(", ");
		sb.append("keyboardDetectionPerWidget");
		sb.append(": ");
		sb.append(isKeyboardDetectionPerWidget());
		return sb.toString();
	}

}
