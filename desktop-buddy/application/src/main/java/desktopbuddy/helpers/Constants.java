package desktopbuddy.helpers;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.model.Jar;
import desktopbuddy.model.SimpleJar;

public enum Constants {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(Constants.class);

	public final String runtimeClasspath = "desktopbuddy.fx.DesktopBuddy";
	public final String runtimePath = OperatingSystem.getWorkFolder() + "runtime" + File.separator;
	public final String widgetsPath = OperatingSystem.getWorkFolder() + "widgets" + File.separator;
	public final String tempFolder = OperatingSystem.getWorkFolder() + "tmp" + File.separator;

	public final String newRuntimeJarPrefix = "new.runtime";
	public final String currentRuntimeJarPrefix = "runtime";
	public Jar runtimeJar;

	public final String newWidgetJarPrefix = "new.widget";
	public final String currentWidgetJarPrefix = "widget";
	public Jar[] widgetsJars;

	public final String updateURL = "https://github.com/franferri/desktop-buddy/releases/latest";
	public final String downloadRuntimeFileNamePrefix = "updates_runtime";
	public final String downloadWidgetFileNamePrefix = "updates_widget";

	private Constants() {
	}

	public void refreshJarsList(boolean deleteOldOnes) {

		try {

			SimpleJar newRuntimeJar = SimpleJar.loadJars(runtimePath, newRuntimeJarPrefix, "", deleteOldOnes);
			SimpleJar currentRuntimeJar = SimpleJar.loadJars(runtimePath, currentRuntimeJarPrefix, "", deleteOldOnes);

			runtimeJar = new Jar(newRuntimeJar, currentRuntimeJar, "");

		} catch (Exception e) {
			log.error("Error loading runtimeJar", e);
		}

		try {

			File[] widgetsFiles = new File(widgetsPath).listFiles((dir, fileName) -> { // widgets names to download have to come from cofig file
				return fileName.startsWith(currentWidgetJarPrefix);
			});

			if (null == widgetsFiles || widgetsFiles.length == 0) {
				return;
			}

			HashSet<String> widgetNames = new HashSet<>();
			for (File file : widgetsFiles) {
				String fileName = file.getName();
				if (fileName.startsWith(newWidgetJarPrefix)) {
					int position = newWidgetJarPrefix.length() + 1;
					widgetNames.add(fileName.substring(position, fileName.indexOf("_", position)));
				} else if (fileName.startsWith(currentWidgetJarPrefix)) {
					int position = currentWidgetJarPrefix.length() + 1;
					widgetNames.add(fileName.substring(position, fileName.indexOf("_", position)));
				} 
			}

			widgetsJars = new Jar[widgetNames.size()];
			int i = 0;
			for (Iterator<String> iterator = widgetNames.iterator(); iterator.hasNext();) {
				String widgetName = (String) iterator.next();

				SimpleJar newWidgetJar = SimpleJar.loadJars(widgetsPath, newWidgetJarPrefix, widgetName, deleteOldOnes);
				SimpleJar currentWidgetJar = SimpleJar.loadJars(widgetsPath, currentWidgetJarPrefix, widgetName, deleteOldOnes);

				widgetsJars[i] = new Jar(newWidgetJar, currentWidgetJar, widgetName);

				++i;
			}

		} catch (Exception e) {
			log.error("Error loading runtimeJar", e);
		}

	}

}