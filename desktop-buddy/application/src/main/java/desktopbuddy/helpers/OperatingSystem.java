package desktopbuddy.helpers;

import java.io.File;

public class OperatingSystem {

	private static final String OS = System.getProperty("os.name").toLowerCase();

	private static String configurationFolder;

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static String getWorkFolder() {

		if (null == configurationFolder) {
			if (isWindows()) {
				configurationFolder = System.getenv("LOCALAPPDATA") + File.separator + "DesktopBuddy" + File.separator;
			} else {
				configurationFolder = System.getProperty("user.home") + File.separator + ".desktopbuddy" + File.separator;
			}
			new File(configurationFolder).mkdirs();
		}

		return configurationFolder;

	}

}
