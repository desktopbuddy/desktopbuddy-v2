package desktopbuddy.helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum FileUnzipper {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(FileUnzipper.class);

	public static void unzip(String folder, String zip, String unzipDir) {
		try {
			new File(unzipDir).mkdirs();

			Long start = new Date().getTime();
			ZipFile zipFile = new ZipFile(folder + zip);

			Enumeration<? extends ZipEntry> entries = zipFile.entries();

			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				if (entry.isDirectory()) {
					System.out.print("dir  : " + entry.getName());
					String destPath = unzipDir + File.separator + entry.getName();
					System.out.println(" => " + destPath);
				} else {
					String destPath = unzipDir + File.separator + entry.getName();

					try (InputStream inputStream = zipFile.getInputStream(entry); FileOutputStream outputStream = new FileOutputStream(destPath);) {
						int data = inputStream.read();
						while (data != -1) {
							outputStream.write(data);
							data = inputStream.read();
						}
					}
					System.out.println("file : " + entry.getName() + " => " + destPath);
				}
			}

			zipFile.close();

			log.debug("Unzip lasted: " + Debug.INSTANCE.millisToHumanFriendly((new Date().getTime() + start)));

		} catch (IOException e) {
			throw new RuntimeException("Error unzipping file " + folder + zip, e);
		}
	}

}
