package desktopbuddy.helpers;

import java.util.concurrent.TimeUnit;

public enum Debug {

	INSTANCE;

	public String millisToHumanFriendly(long millis) {
		return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	}

}
