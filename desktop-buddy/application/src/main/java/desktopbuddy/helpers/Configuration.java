package desktopbuddy.helpers;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import desktopbuddy.helpers.configuration.Config;

public enum Configuration {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(Configuration.class);

	private static final String configFile = "config.json";
	private static final Gson gson = new GsonBuilder().create();

	private static Config config = new Config();

	public void configSave() {

		try (Writer writer = new FileWriter(OperatingSystem.getWorkFolder() + configFile)) {
			gson.toJson(config, writer);
			writer.close(); // We make sure we flush to the file the changes
		} catch (IOException e) {
			log.debug("Problem saving the configuration file", e);
		}

	}

	public void configReload() {

		if (!(new File(OperatingSystem.getWorkFolder() + configFile).exists())) {
			configSave();
		}

		try (Reader reader = new FileReader(OperatingSystem.getWorkFolder() + configFile)) {
			config = gson.fromJson(reader, Config.class);
			log.debug(config);
		} catch (IOException e) {
			log.debug("Problem reading the configuration file", e);
		}
		
	}

	public void configSaveAndReload() {
		configSave();
		configReload();
	}

	public Config config() {
		return config;
	}

}
