package desktopbuddy.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;

public class App {

	public static final Logger log = LogManager.getLogger(App.class);

	public static final String SHUTDOWN_HOOK = "Shutdown hook: ";

	public static void shutdown(int statusCode) {
		log.debug("Shutdown started");
		Platform.exit();
		System.runFinalization();
		System.exit(statusCode);
	}

}
