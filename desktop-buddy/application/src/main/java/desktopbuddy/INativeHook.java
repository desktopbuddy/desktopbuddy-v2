package desktopbuddy;

import java.util.EventListener;

public abstract class INativeHook {

	public abstract boolean isGlobalHotKeysDetectionEnabled();

	public abstract void registerDriver();

	public abstract void enableGlobalHotkeysDetection(EventListener mappings);

	public abstract void disableGlobalHotkeysDetection(EventListener mappings);
}
