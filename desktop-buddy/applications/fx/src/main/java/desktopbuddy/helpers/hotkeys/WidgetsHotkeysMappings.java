package desktopbuddy.helpers.hotkeys;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import desktopbuddy.helpers.Configuration;
import desktopbuddy.helpers.configuration.Widget;

public class WidgetsHotkeysMappings extends HotkeysMappings {

	// Si la combinación de los widgets se repite podríamos hacer que al pulsarla otra vez rotamos entre los diferentes widgets
	public WidgetsHotkeysMappings() {
		// We read the combination from the configuraiton
		// We map the method to call to the combination

		List<Widget> widgets = Configuration.INSTANCE.config().widgets();
		for (Widget widget : widgets) {
			Integer[] _keyCodes = widget.getHotkeys();
			if (null == _keyCodes || _keyCodes.length == 0) {
				log.info("There is no key combination, using the default: LEFT_SHIFT ESC");
				_keyCodes = new Integer[] { 1, 42 };
			}

			Set<Integer> keyCodes = new HashSet<>(Arrays.asList(_keyCodes));
			setMapping(keyCodes, WidgetsHotkeysMappings::mec);

		}

	}

	private static void mec() {
		log.debug("widget");
	}

}
