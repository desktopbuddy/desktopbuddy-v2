package desktopbuddy.helpers;

import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Images {

	public static final Logger log = LogManager.getLogger(Images.class);

	public static Image resizeImage(Image image, Dimension availableSpace) {
		Integer width = Double.valueOf(availableSpace.getWidth()).intValue();
		Integer height = Double.valueOf(availableSpace.getHeight()).intValue();
		return resizeImage(image, width, height);
	}

	public static Image resizeImage(Image image, int width, int height) {
		try {
			return image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		} catch (Exception e) {
			log.error("resizeImage: " + e.getMessage());
			return image;
		}
	}

	public static Image findBestImage(int size) throws IOException {

		LinkedHashMap<Integer, String> images = new LinkedHashMap<>();
		images.put(16, "icon_16px.png");
		images.put(32, "icon_32px.png");
		images.put(48, "icon_48px.png");
		images.put(128, "icon_128px.png");
		images.put(256, "icon_256px.png");
		images.put(512, "icon_512px.png");

		Iterator<Entry<Integer, String>> it = images.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, String> pair = (Entry<Integer, String>) it.next();
			if (pair.getKey() >= size) {
				log.debug("Image: " + pair.getValue());

				ImageIcon imageIcon = new ImageIcon(ImageIO.read(Images.class.getClassLoader().getResourceAsStream(pair.getValue())), "");
				if (pair.getKey() != size) {
					log.debug("Reducing the size of the image " + pair.getValue() + " to " + size + "px");
					return resizeImage(imageIcon.getImage(), size, size);
				} else {
					return imageIcon.getImage();
				}

			}
		}

		return new ImageIcon(ImageIO.read(Images.class.getClassLoader().getResourceAsStream("icon_512px.png")), "").getImage();
	}

}
