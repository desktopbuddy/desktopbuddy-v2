package desktopbuddy.helpers;

import java.util.EventListener;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyListener;

public class NativeHook {

	public static final Logger log = LogManager.getLogger(NativeHook.class);

	private static boolean isGlobalHotKeysDetectionEnabled = false;
	private static Set<EventListener> activeMappings = new HashSet<>();

	private static void register() throws NativeHookException {

		log.info("Registering native hook");
		GlobalScreen.registerNativeHook();

		if (GlobalScreen.isNativeHookRegistered()) {
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					unregisterNativeHook();
				}
			});
		}

	}

	// In windows and linux this causes a break, we don't unregister the keyboard hook when is registered, we remove the mappings
	@Deprecated
	private static void unregisterNativeHook() {
		try {
			log.info(App.SHUTDOWN_HOOK + "Unregistering native hook");
			GlobalScreen.unregisterNativeHook();
		} catch (NativeHookException e) {
			log.error(App.SHUTDOWN_HOOK + "Error unregistering native hook", e);
		} catch (Exception e) {
			log.error(App.SHUTDOWN_HOOK + "Unknown error", e);
		} catch (Throwable e) {
			log.error(App.SHUTDOWN_HOOK + "Unknown internal error", e);
		}
	}

	private static boolean isDriverRegistered() {
		return GlobalScreen.isNativeHookRegistered();
	}

	public static boolean isGlobalHotKeysDetectionEnabled() {
		return isGlobalHotKeysDetectionEnabled;
	}

	public static void registerDriver() {
		log.debug("registerDriver NativeKey");
		try {
			if (!NativeHook.isDriverRegistered()) {
				NativeHook.register();
			}
		} catch (NativeHookException e) {
			log.error("There was a problem registering the native hook driver.", e);
		}

	}

	private static void enableHotkeysDetection(EventListener mapping) {
		log.debug("EnabledGlobalHotkeysDetection");
		if (!NativeHook.isDriverRegistered()) {
			return;
		}

		activeMappings.add(mapping);
		GlobalScreen.addNativeKeyListener((NativeKeyListener) mapping);
		isGlobalHotKeysDetectionEnabled = true;

	}

	private static void disableHotkeysDetection() {
		if (!NativeHook.isDriverRegistered()) {
			return;
		}

		log.debug("DisabledGlobalHotkeysDetection");
		for (EventListener mapping : activeMappings) {
			GlobalScreen.removeNativeKeyListener((NativeKeyListener) mapping);
		}
		activeMappings.clear();
		isGlobalHotKeysDetectionEnabled = false;
	}

	public static void enableGlobalHotkeysDetection(EventListener mapping) {
		disableHotkeysDetection();
		enableHotkeysDetection(mapping);
	}

	public static void enableWidgetsHotkeysDetection(EventListener mapping) {
		disableHotkeysDetection();
		enableHotkeysDetection(mapping);
	}

	public static void recordHotkeysMapping() {

	}

}
