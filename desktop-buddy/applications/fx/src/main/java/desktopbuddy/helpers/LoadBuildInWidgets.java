package desktopbuddy.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.fx.DesktopBuddy;
import desktopbuddy.views.ViewHotKeys;
import desktopbuddy.views.ViewOptions;
import desktopbuddy.views.ViewWidgetOptions;
import desktopbuddy.views.ViewWidgetsList;
import desktopbuddy.views.ViewWidgetsSelector;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public enum LoadBuildInWidgets {

	// https://gist.github.com/humblenut/f92a5854eedcde4be520
	INSTANCE;

	public static final Logger log = LogManager.getLogger(LoadBuildInWidgets.class);

	private Stage viewWidgetSelector;
	private Stage viewOptions;
	private Stage viewWidgetsList;
	private Stage viewHotKeys;
	private Stage viewWidgetOptions;

	public static boolean viewWidgetSelectorEscPressed = false;

	private void loadViewWidgetsSelector() {
		viewWidgetSelector = new Stage();

		viewWidgetSelector.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				System.out.println("viewWidgetSelectorEscPressed: " + viewWidgetSelectorEscPressed);
				if (viewWidgetSelectorEscPressed) {
					viewWidgetSelectorEscPressed = false;
					viewWidgetSelector.hide();
				} else {
					viewWidgetSelectorEscPressed = true;
					focusAndSelectAll();
				}
			} else {
				viewWidgetSelectorEscPressed = false;
			}
		});

		loadStage(viewWidgetSelector, "WidgetSelector.fxml", new ViewWidgetsSelector());
	}

	public void showViewWidgetsSelector() {
		Platform.runLater(() -> {
			if (null == viewWidgetSelector) {
				loadViewWidgetsSelector();
			}
			focusAndSelectAll();
			viewWidgetSelector.setAlwaysOnTop(true);
			viewWidgetSelector.show();
		});
	}

	private void focusAndSelectAll() {
		// Escape key to focus (we need to unfocus, and focus to achieve full selecton
		Node box = viewWidgetSelector.getScene().lookup("#searchBox");
		box.requestFocus();
		((TextField) box).selectAll();
	}

	private void loadViewOptions() {
		viewOptions = new Stage();

		viewOptions.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				viewOptions.hide();
			}
		});

		loadStage(viewOptions, "Options.fxml", new ViewOptions());
	}

	public void showViewOptions() {
		Platform.runLater(() -> {
			if (null == viewOptions) {
				loadViewOptions();
			}
			viewOptions.show();
		});
	}

	private void loadViewWidgetsList() {
		viewWidgetsList = new Stage();

		viewWidgetsList.initModality(Modality.APPLICATION_MODAL);

		viewWidgetsList.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				viewWidgetsList.hide();
			}
		});

		loadStage(viewWidgetsList, "WidgetsList.fxml", new ViewWidgetsList());
	}

	public void showViewWidgetsList() {
		Platform.runLater(() -> {
			if (null == viewWidgetsList) {
				loadViewWidgetsList();
			}
			viewWidgetsList.show();
		});
	}

	private void loadViewHotKeys() {
		viewHotKeys = new Stage();

		viewHotKeys.initModality(Modality.APPLICATION_MODAL);

		viewHotKeys.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				viewHotKeys.hide();
			}
		});

		loadStage(viewHotKeys, "Hotkeys.fxml", new ViewHotKeys());
	}

	public void showViewHotKeys() {
		Platform.runLater(() -> {
			if (null == viewHotKeys) {
				loadViewHotKeys();
			}
			viewHotKeys.show();
		});
	}

	private void loadViewWidgetOptions() {
		viewWidgetOptions = new Stage();

		viewWidgetOptions.initModality(Modality.APPLICATION_MODAL);

		viewWidgetOptions.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				viewWidgetOptions.hide();
			}
		});

		loadStage(viewWidgetOptions, "WidgetOptions.fxml", new ViewWidgetOptions());
	}

	public void showViewWidgetOptions() {
		Platform.runLater(() -> {
			if (null == viewWidgetOptions) {
				loadViewWidgetOptions();
			}
			viewWidgetOptions.show();
		});
	}

	public void load() {
		loadViewWidgetsSelector();
		loadViewOptions();
		loadViewHotKeys();
		loadViewWidgetsList();
		loadViewWidgetOptions();
	}

	private void loadStage(Stage stage, String fxmlName, Object viewController) {

		Platform.runLater(() -> {
			log.debug("Loading Options View");
			try {
				FXMLLoader loader = new FXMLLoader(DesktopBuddy.class.getClassLoader().getResource(fxmlName));

				// Controller
				loader.setController(viewController);
				Parent parent = loader.load();

				// Scene
				Scene scene = new Scene(parent);
				scene.getStylesheets().add(DesktopBuddy.class.getClassLoader().getResource("cross_platform.css").toExternalForm());

				// Behaviour
				stage.setResizable(false);
				stage.initStyle(StageStyle.UTILITY);// Has to be utiliy or it will not get the focus when it appears op top https://www.namekdev.net/2016/03/javafx-taskbar-less-undecorated-window/

				stage.setScene(scene);
				stage.hide();
			} catch (Exception e) {
				log.error("Error loading Options: ", e);
			}
		});
	}

}
