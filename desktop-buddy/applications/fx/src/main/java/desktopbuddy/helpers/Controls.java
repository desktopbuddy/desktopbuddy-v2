package desktopbuddy.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.Notifications;

import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public enum Controls {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(Controls.class);

	public void notification() {

		try {

			Stage dummyPopup = new Stage();
			dummyPopup.initModality(Modality.NONE);
			// set as utility so no iconification occurs
			dummyPopup.initStyle(StageStyle.UTILITY);
			// set opacity so the window cannot be seen
			dummyPopup.setOpacity(0d);
			// not necessary, but this will move the dummy stage off the screen
			final Screen screen = Screen.getPrimary();
			final Rectangle2D bounds = screen.getVisualBounds();
			dummyPopup.setX(bounds.getMaxX());
			dummyPopup.setY(bounds.getMaxY());
			// create/add a transparent scene
			final Group root = new Group();
			dummyPopup.setScene(new Scene(root, 1d, 1d, Color.TRANSPARENT));
			// show the dummy stage
			dummyPopup.show();

		} catch (Exception e) {
			log.error("Error loading Notifications stage: ", e);
		}

		Platform.runLater(() -> {
			Image image = new Image(Controls.class.getClassLoader().getResourceAsStream("icon_128px.png"));
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			try {
				Notifications.create().title(":D").text("DesktopBuddy running").hideAfter(new Duration(5000)).graphic(imageView).show();
			} catch (Exception e) {
				log.error("Error showing notification, screen not detected (in a laptop maybe the lid was closed).");
			}
		});

	}

}
