package desktopbuddy.helpers;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.fx.DesktopBuddy;
import desktopbuddy.widget.DesktopBuddyWidget;

public enum LoadWidgets {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(LoadWidgets.class);

	public static final String widgetsPath = OperatingSystem.getWorkFolder() + "widgets" + File.separator;

	public void loadWidgets() {

		String classpath = "desktopbuddy.widgets.Main";

		try {
			List<DesktopBuddyWidget> widgets = Reflect.loadClassCheckingTypeAndInstantiate(widgetsPath, classpath, DesktopBuddyWidget.class, DesktopBuddy.class.getClassLoader());

			for (DesktopBuddyWidget widget : widgets) {
				widget.helloWorld();
			}

		} catch (Exception e) {
			log.error("Problem loading the jar with the class " + classpath, e);
			// Application.shutdown(1);
		}

	}

}
