package desktopbuddy.helpers.hotkeys;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import desktopbuddy.helpers.Configuration;
import desktopbuddy.helpers.LoadBuildInWidgets;

public class GlobalHotkeysMappings extends HotkeysMappings {

	// we link the event of the key pressed to the window we want to open

	public GlobalHotkeysMappings() {

		// We read the combination from the configuraiton
		// We map the method to call to the combination

		Integer[] _keyCodes = Configuration.INSTANCE.config().globalHotkeys();
		if (null == _keyCodes || _keyCodes.length == 0) {
			log.info("There is no key combination, using the default: LEFT_SHIFT < z");
			_keyCodes = new Integer[] { 56, 40, 3675 };
		}

		Set<Integer> keyCodes = new HashSet<>(Arrays.asList(_keyCodes));
		setMapping(keyCodes, GlobalHotkeysMappings::loadWidget);

	}

	private static void loadWidget() {
		LoadBuildInWidgets.INSTANCE.showViewWidgetsSelector();
	}

}
