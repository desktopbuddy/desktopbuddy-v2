package desktopbuddy.helpers;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum NativeSystemTray {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(NativeSystemTray.class);
	private TrayIcon trayIcon;
	private SystemTray tray;

	public void setupSystemTray() {
		try {
			if (!SystemTray.isSupported()) {
				log.debug("Current OS systemtray is not supported");
				return;
			}
			tray = SystemTray.getSystemTray();
			Dimension availableSpace = tray.getTrayIconSize();
			Image icon = Images.findBestImage(Double.valueOf(availableSpace.getHeight()).intValue());
			trayIcon = new TrayIcon(icon, "Desktop Buddy");
			tray.add(trayIcon);
			detectMouseClicks();
		} catch (Exception e) {
			log.error("There was an error handling the system tray", e);
		}
	}

	public TrayIcon getTrayIcon() {
		return trayIcon;
	}

	private void detectMouseClicks() {

		trayIcon.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 1) {
					log.debug("TrayIcon: Left - Click - Open widget selector");
					LoadBuildInWidgets.INSTANCE.showViewWidgetsSelector();
				}
			}
		});

		trayIcon.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				if (event.getButton() == MouseEvent.BUTTON3 && event.getClickCount() == 1) {
					log.debug("TrayIcon: Right - Click - Open options");
					LoadBuildInWidgets.INSTANCE.showViewOptions();
				}
			}
		});

	}

}
