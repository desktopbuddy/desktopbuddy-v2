package desktopbuddy.helpers.hotkeys;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class HotkeysMappings implements NativeKeyListener {

	public static final Logger log = LogManager.getLogger(HotkeysMappings.class);

	private Set<Integer> currentPressedKeys = new HashSet<>();
	private Set<String> currentPressedKeysRepresentation = new HashSet<>();

	private Map<Set<Integer>, Runnable> mappings = new HashMap<>();

	@Override
	public void nativeKeyTyped(NativeKeyEvent event) {
		// don't use
	}

	Integer[] _pressedKeys;
	StringBuilder sb;

	@Override
	public void nativeKeyPressed(NativeKeyEvent event) {

		String key = keyCodeToString(event.getKeyCode());
		if (null == key) {
			return;
		}

		currentPressedKeys.add(event.getKeyCode());
		currentPressedKeysRepresentation.add(key);

		debug();

		runMappings();

	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent event) {

		String key = keyCodeToString(event.getKeyCode());
		if (null == key) {
			return;
		}

		currentPressedKeys.remove(event.getKeyCode());

	}

	private String keyCodeToString(int keyCode) {
		String key = NativeKeyEvent.getKeyText(keyCode);
		if (key.startsWith("Unknown") || keyCode == NativeKeyEvent.VC_UNDEFINED) {
			return null;
		}
		return key;
	}

	public void setMapping(Set<Integer> keyCodes, Runnable method) {
		mappings.put(keyCodes, method);
	}

	private void runMappings() {

		Runnable runnable = null;
		for (Set<Integer> keyCodes : mappings.keySet()) {
			if (areEquals(keyCodes, currentPressedKeys)) {
				runnable = mappings.get(keyCodes);
				runnable.run();
			}
		}

	}

	public static boolean areEquals(Set<?> set1, Set<?> set2) {
		if (set1 == null || set2 == null) {
			return false;
		}
		if (set1.size() != set2.size()) {
			return false;
		}
		return set1.containsAll(set2);
	}

	private void debug() {
		_pressedKeys = currentPressedKeys.toArray(new Integer[0]);
		sb = new StringBuilder();
		for (int i = 0; i < _pressedKeys.length; i++) {
			sb.append(keyCodeToString(_pressedKeys[i]));
			if (i != _pressedKeys.length - 1) {
				sb.append(", ");
			}
		}
		log.debug("Pressing " + currentPressedKeys.size() + " keys: " + currentPressedKeys.toString() + " => " + sb.toString());
	}

}
