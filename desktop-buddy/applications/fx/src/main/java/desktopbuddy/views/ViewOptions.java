package desktopbuddy.views;

import java.net.URL;
import java.util.ResourceBundle;

import desktopbuddy.helpers.LoadBuildInWidgets;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

public class ViewOptions implements Initializable {

	@FXML
	private Button btnAddNewWidgets;

	public ViewOptions() {
	}

	public void initialize(URL location, ResourceBundle resources) {
	}

	@FXML
	void openWidgetsList(ActionEvent event) {
		LoadBuildInWidgets.INSTANCE.showViewWidgetsList();
	}

	@FXML
	void openHotkeys(ActionEvent event) {
		LoadBuildInWidgets.INSTANCE.showViewHotKeys();
	}

	@FXML
	void openWidgetOptions(ActionEvent event) {
		LoadBuildInWidgets.INSTANCE.showViewWidgetOptions();
	}

}
