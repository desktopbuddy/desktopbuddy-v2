package desktopbuddy.fx;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.helpers.Configuration;
import desktopbuddy.helpers.Controls;
import desktopbuddy.helpers.LoadBuildInWidgets;
import desktopbuddy.helpers.LoadWidgets;
import desktopbuddy.helpers.NativeHook;
import desktopbuddy.helpers.NativeSystemTray;
import desktopbuddy.helpers.hotkeys.GlobalHotkeysMappings;
import desktopbuddy.helpers.hotkeys.WidgetsHotkeysMappings;
import javafx.application.Platform;
import javafx.stage.Stage;

public class DesktopBuddy extends javafx.application.Application {

	public static final Logger log = LogManager.getLogger(DesktopBuddy.class);
	private static boolean keybardDetection;

	@Override
	public void init() throws Exception {
		super.init();
	}

	@Override
	public void start(Stage primaryStage) {
		Parameters parameters = getParameters();
		if (parameters.getRaw().contains("options")) {
			openOptionsOnSecondClick(); // For non system tray supported OS by java
		} else {
			normalMode();
		}
	}

	private void openOptionsOnSecondClick() {
		Platform.setImplicitExit(true);
		LoadBuildInWidgets.INSTANCE.showViewOptions();
	}

	private void normalMode() {

		log.info("Desktop buddy running");

		Platform.setImplicitExit(false);

		Controls.INSTANCE.notification();
		NativeSystemTray.INSTANCE.setupSystemTray();

		keybardDetection = Configuration.INSTANCE.config().isEnableKeybardDetection();
		log.debug("Is sytem keyboard detection enabled?: " + keybardDetection);
		if (keybardDetection) {
			NativeHook.registerDriver();
		}

		try {
			LoadBuildInWidgets.INSTANCE.load();
			// We need only to load all widgets from the configuration
			// The remaining will be listed in the options view
			LoadWidgets.INSTANCE.loadWidgets();
		} catch (Exception e) {
			log.error("Error lading widgets", e);
		}

		if (Configuration.INSTANCE.config().isKeyboardDetectionPerWidget()) {
			NativeHook.enableWidgetsHotkeysDetection(new WidgetsHotkeysMappings());
		} else {
			NativeHook.enableGlobalHotkeysDetection(new GlobalHotkeysMappings());
		}

	}

}
