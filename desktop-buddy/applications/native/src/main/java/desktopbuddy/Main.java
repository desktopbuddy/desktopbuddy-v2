package desktopbuddy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.helpers.AllowOnlyOneInstance;
import desktopbuddy.helpers.App;
import desktopbuddy.helpers.Configuration;
import desktopbuddy.helpers.Constants;
import desktopbuddy.helpers.LoadRuntime;
import desktopbuddy.helpers.Logging;
import desktopbuddy.helpers.OperatingSystem;
import desktopbuddy.helpers.Updates;
import javafx.application.Application;

public class Main {

	static {
		Logging.INSTANCE.init(OperatingSystem.getWorkFolder());
	}

	public static final Logger log = LogManager.getLogger(Main.class);
	private static boolean autoUpdates;

	public static void main(String[] args) {

		try {
			if (OperatingSystem.isMac()) {
				java.awt.Toolkit.getDefaultToolkit(); // MacOS needed to avoid Dock icon (https://stackoverflow.com/questions/24312260/javafx-application-hide-osx-dock-icon/32984600#32984600)
			}

			AllowOnlyOneInstance.lock();

			Configuration.INSTANCE.configReload();

			autoUpdates = Configuration.INSTANCE.config().isAutoUpdateEnabled();
			log.debug("Auto-updates are enabled?: " + autoUpdates);
			if (autoUpdates) {
				Updates.INSTANCE.autoUpdate();
			}

			loadFXEnvironment(false);

			App.shutdown(0);
		} catch (Throwable t) {
			log.debug("Error while running", t);
			App.shutdown(1);
		}

	}

	public static void loadFXEnvironment(boolean showOptions) {

		Constants.INSTANCE.refreshJarsList(true); // Load the available jars in memory

		Class<Application> runtime = LoadRuntime.INSTANCE.loadRuntime();
		if (null == runtime) {
			log.error("There is no runtime available. Aborting.");
			App.shutdown(1);
		}

		log.debug("Loading visual environment");
		if (showOptions) {
			Application.launch(runtime, "options");
		} else {
			Application.launch(runtime); // MacOS needed to avoid Dock icon (https://stackoverflow.com/questions/24312260/javafx-application-hide-osx-dock-icon/32984600#32984600)
		}

	}

}
