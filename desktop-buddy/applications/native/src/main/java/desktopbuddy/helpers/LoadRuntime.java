package desktopbuddy.helpers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.Main;
import desktopbuddy.model.Jar;
import javafx.application.Application;

public enum LoadRuntime {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(LoadRuntime.class);

	public Class<Application> loadRuntime() {

		LoadRuntime.INSTANCE.refreshWidgets();

		Class<Application> runtimeJar = null;

		if (Constants.INSTANCE.runtimeJar.newJar().exists()) {
			log.debug("Detected new runtime version file stored locally");

			boolean updatedCurrent = activateNewVersion(Constants.INSTANCE.runtimeJar.newJar());

			Constants.INSTANCE.refreshJarsList(true); // Load the available jars in memory
			runtimeJar = validateAndLoadJar(Constants.INSTANCE.runtimeJar.currentJar());
			boolean validated = (null != runtimeJar);

			if (validated && updatedCurrent) {
				return runtimeJar;
			}

			log.debug("Trying to unloading new runtime version from classpath");
			runtimeJar = null;
			System.gc();
		}

		Constants.INSTANCE.refreshJarsList(true);
		if (!Constants.INSTANCE.runtimeJar.currentJar().exists()) {
			downloadRuntime();
		}

		Constants.INSTANCE.refreshJarsList(true);
		if (Constants.INSTANCE.runtimeJar.currentJar().exists()) {
			runtimeJar = validateAndLoadJar(Constants.INSTANCE.runtimeJar.currentJar());
			if (runtimeJar == null) {
				downloadRuntime();
			}
		}

		Constants.INSTANCE.refreshJarsList(true);
		if (Constants.INSTANCE.runtimeJar.currentJar().exists()) {
			log.debug("Loading current runtime " + Constants.INSTANCE.runtimeJar.currentJar().getAbsolutePath());
			return validateAndLoadJar(Constants.INSTANCE.runtimeJar.currentJar());
		}

		return runtimeJar;

	}

	public void downloadRuntime() {
		log.debug("Downloading a valid runtime");
		Updates.INSTANCE.dowloadRuntimeFromUpdates();
		Constants.INSTANCE.refreshJarsList(true);
		activateNewVersion(Constants.INSTANCE.runtimeJar.newJar());
	}

	private boolean activateNewVersion(File newRuntimeJar) {
		String renamed;
		log.debug("Making the new runtime version the default runtime");
		renamed = newRuntimeJar.getParent() + File.separator + newRuntimeJar.getName().substring(4, newRuntimeJar.getName().length());
		boolean updatedCurrent = moveFileOverwrite(newRuntimeJar, new File(renamed));
		return updatedCurrent;
	}

	private Class<Application> validateAndLoadJar(File file) {
		try {
			log.debug("Validating the runtime version file");
			return (Class<Application>) Reflect.loadClassCheckingType(file, Constants.INSTANCE.runtimeClasspath, Application.class, Main.class.getClassLoader());
		} catch (Exception e) {
			log.debug("The runtime version file is corrupted", e);
			try {
				log.debug("Deleting the corrupted runtime version file");
				file.delete();
			} catch (Exception ex) {
				log.debug("Error deleting the corrupted runtime version file", ex);
			}
		}
		return null;
	}

	private boolean moveFileOverwrite(File source, File destiny) {
		boolean updatedOld = false;
		try {
			Files.move(source.toPath(), destiny.toPath(), StandardCopyOption.REPLACE_EXISTING);
			updatedOld = true;
		} catch (Exception e) {
			log.debug("The file can't be moved", e.getMessage());
		}
		return updatedOld;
	}

	public void refreshWidgets() {

		if (null != Constants.INSTANCE.widgetsJars) {
			for (Jar jar : Constants.INSTANCE.widgetsJars) {
				if (jar.newJar().exists()) {
					activateNewVersion(jar.newJar());
				}
			}
		}

	}

}
