package desktopbuddy.helpers;

import java.io.File;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.LayoutComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

public enum Logging {

	INSTANCE;

	static {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
	}

	public static final String PATTERN = "%d [%t] %-5level: %msg%n%throwable";
	public static final String LOGFILE = "logs" + File.separator + "desktopbuddy.log";// OperatingSystem.INSTANCE.getWorkFolder();
	public static final String LOGFILEPATTERN = "logs" + File.separator + "desktopbuddy-%d{yyyy-MM-dd}-%i.log.gz";

	public void init(String logsFolder) {

		File folder = new File(logsFolder);
		if (!folder.isDirectory() || !folder.exists()) {
			System.out.println("Logs folder is not valid directory or don't exists");
			App.shutdown(1);
		}

		ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
		builder.setStatusLevel(Level.WARN);

		builder.setConfigurationName("DesktopBuddy");

		// create a console appender
		AppenderComponentBuilder appenderBuilder = builder.newAppender("Console", "CONSOLE").addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);
		appenderBuilder.add(builder.newLayout("PatternLayout").addAttribute("pattern", PATTERN));
		builder.add(appenderBuilder);

		// create a rolling file appender
		LayoutComponentBuilder layoutBuilder = builder.newLayout("PatternLayout").addAttribute("pattern", PATTERN);
		ComponentBuilder<?> triggeringPolicy = builder.newComponent("Policies").addComponent(builder.newComponent("SizeBasedTriggeringPolicy").addAttribute("size", "5M"));
		ComponentBuilder<?> rolloverPolicy = builder.newComponent("DefaultRolloverStrategy").addAttribute("max", "5");
		appenderBuilder = builder.newAppender("RollingFile", "RollingFile").addAttribute("fileName", logsFolder + LOGFILE).addAttribute("filePattern", logsFolder + LOGFILEPATTERN).add(layoutBuilder).addComponent(triggeringPolicy).addComponent(rolloverPolicy);
		builder.add(appenderBuilder);

		// create the new logger
		builder.add(builder.newLogger("org.jnativehook", Level.FATAL).addAttribute("additivity", false).add(builder.newAppenderRef("Console")).add(builder.newAppenderRef("RollingFile")));

		// create the root logger
		builder.add(builder.newRootLogger(Level.DEBUG).addAttribute("additivity", false).add(builder.newAppenderRef("Console")).add(builder.newAppenderRef("RollingFile")));

		LoggerContext ctx = Configurator.initialize(builder.build());

	}

}
