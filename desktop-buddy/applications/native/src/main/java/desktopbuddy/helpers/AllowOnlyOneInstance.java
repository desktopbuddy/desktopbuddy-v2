package desktopbuddy.helpers;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.Main;

public class AllowOnlyOneInstance {

	public static final Logger log = LogManager.getLogger(AllowOnlyOneInstance.class);

	public static final String lockFile = "desktopbuddy.lockfile";
	private static boolean locked;

	public static void lock() {

		locked = lockInstance(lockFile);
		if (!locked) {
			Main.loadFXEnvironment(true);
			throw new RuntimeException("Only one running instance is allowed");
		}

	}

	private static boolean lockInstance(final String lockFile) {
		try {
			log.debug("Locking the instance");
			final File file = new File(OperatingSystem.getWorkFolder() + lockFile);
			final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
			final FileLock fileLock = randomAccessFile.getChannel().tryLock();
			if (fileLock != null) {
				Runtime.getRuntime().addShutdownHook(new Thread() {
					public void run() {
						try {
							log.debug(App.SHUTDOWN_HOOK + "Removing lock file");
							fileLock.release();
							randomAccessFile.close();
							file.delete();
						} catch (Exception e) {
							log.error(App.SHUTDOWN_HOOK + "Unable to remove lock file: " + lockFile, e);
						}
					}
				});
				return true;
			}
		} catch (Exception e) {
			log.error("Unable to create and/or lock file: " + lockFile, e);
		}
		return false;
	}

}
