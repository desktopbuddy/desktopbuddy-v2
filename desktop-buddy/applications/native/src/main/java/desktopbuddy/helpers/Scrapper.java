package desktopbuddy.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public enum Scrapper {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(Scrapper.class);

	public String scrapURL(String prefix) {
		try {
			log.debug("Accessing " + prefix);

			Document doc = Jsoup.connect(Constants.INSTANCE.updateURL).get();
			String searchPattern = "a[href*=" + prefix + "]";
			log.debug(searchPattern);
			Elements links = doc.select(searchPattern);

			if (!links.isEmpty()) {
				Element link = links.get(0);
				String href = link.absUrl("href");
				log.debug(href);
				return href;
			}
		} catch (Exception e) {
			log.error("Error downloading the " + prefix + " version", e);
		}
		return null;
	}

}
