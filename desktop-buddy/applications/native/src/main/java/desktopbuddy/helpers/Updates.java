package desktopbuddy.helpers;

import java.io.File;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import desktopbuddy.model.Jar;

public enum Updates {

	INSTANCE;

	public static final Logger log = LogManager.getLogger(Updates.class);

	public static String prefix = Constants.INSTANCE.downloadRuntimeFileNamePrefix + "_";
	
	public void autoUpdate() {

		Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
			checkForUpdates();
		}, 300, 1800, TimeUnit.SECONDS);

	}

	public void checkForUpdates() {
		try {
			log.debug("Checking for updates in background...");

			Constants.INSTANCE.refreshJarsList(false);
			detectNewRuntimeVersionAndDownload();

			Constants.INSTANCE.refreshJarsList(false);
			detectNewWidgetsVersionAndDownload();

			Constants.INSTANCE.refreshJarsList(false);

		} catch (Exception e) {
			log.error("Impossible to check for updates", e);
		}
	}

	private void detectNewRuntimeVersionAndDownload() {

		if (null == Constants.INSTANCE.runtimeJar) {
			return;
		}

		boolean newVersion = Constants.INSTANCE.runtimeJar.newJar().exists();
		Long newRuntimeVersion = Constants.INSTANCE.runtimeJar.newJarVersion();
		log.debug("New version: " + newRuntimeVersion);

		Long currentRuntimeVersion = Constants.INSTANCE.runtimeJar.currentJarVersion();
		log.debug("Current runtime version: " + currentRuntimeVersion);

		Long onlineRuntimeVersion = detectOnlineVersion(prefix);
		log.debug("Online runtime version: " + onlineRuntimeVersion);

		if (newVersion && onlineRuntimeVersion > newRuntimeVersion) {
			dowloadRuntimeFromUpdates();
		} else if (!newVersion && onlineRuntimeVersion > currentRuntimeVersion) {
			dowloadRuntimeFromUpdates();
		}

	}

	private void detectNewWidgetsVersionAndDownload() {

		if (null == Constants.INSTANCE.widgetsJars) {
			return;
		}

		for (Jar jar : Constants.INSTANCE.widgetsJars) {

			boolean newVersion = jar.newJar().exists();
			Long newWidgetVersion = jar.newJarVersion();
			log.debug("New widget: " + jar.name() + " version: " + newWidgetVersion);

			Long currentWidgetVersion = jar.currentJarVersion();
			log.debug("Current widget " + jar.name() + " version: " + currentWidgetVersion);

			String prefix = Constants.INSTANCE.downloadWidgetFileNamePrefix + "_" + jar.name() + "_";
			Long onlineWidgetVersion = detectOnlineVersion(prefix);
			log.debug("Online widget: " + jar.name() + " version: " + onlineWidgetVersion);

			if (newVersion && onlineWidgetVersion > newWidgetVersion) {
				dowloadWidgetsFromUpdates(prefix, jar.name());
			} else if (!newVersion && onlineWidgetVersion > currentWidgetVersion) {
				dowloadWidgetsFromUpdates(prefix, jar.name());
			}

		}

	}

	public void dowloadRuntimeFromUpdates() {
		File downloadedFile = new File(Constants.INSTANCE.tempFolder + Constants.INSTANCE.downloadRuntimeFileNamePrefix + ".zip");
		File zip = download(downloadedFile, prefix);
		unZipFileFromTemp(zip.getName(), Constants.INSTANCE.runtimePath);
	}

	private void dowloadWidgetsFromUpdates(String prefix, String name) {
		File downloadedFile = new File(Constants.INSTANCE.tempFolder + Constants.INSTANCE.downloadWidgetFileNamePrefix + "_" + name + ".zip");
		File zip = download(downloadedFile, prefix);
		unZipFileFromTemp(zip.getName(), Constants.INSTANCE.widgetsPath);
	}

	private File download(File downloadedFile, String prefix) {

		String href = Scrapper.INSTANCE.scrapURL(prefix);
		try {
			if (null != href) {
				URL url = new URL(href);
				FileUtils.copyURLToFile(url, downloadedFile, 5000, 5000);
			}
		} catch (Exception e) {
			log.error("Error downloading the " + prefix + " version. This could b due no internet connection available at the instant the update was checked.", e);
		}
		return downloadedFile;

	}

	private Long detectOnlineVersion(String prefix) {
		String href = Scrapper.INSTANCE.scrapURL(prefix);
		try {
			if (null != href) {
				int versionPosition = href.indexOf(prefix) + prefix.length();
				String version = href.substring(versionPosition, href.length() - 4);
				log.debug("Online version: " + version);
				return Long.valueOf(version);
			}
		} catch (Exception e) {
			log.error("Error getting the new version", e);
		}
		return 0l;
	}

	private void unZipFileFromTemp(String fileName, String destiny) {
		try {
			log.debug("Uncompressing file " + fileName);
			FileUnzipper.unzip(Constants.INSTANCE.tempFolder, fileName, destiny);
			new File(Constants.INSTANCE.tempFolder + fileName).delete();
		} catch (Exception e) {
			log.error("Error uncompressing the file " + fileName, e);
		}
	}

}
