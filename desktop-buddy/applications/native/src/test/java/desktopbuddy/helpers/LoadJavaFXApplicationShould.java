package desktopbuddy.helpers;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

public class LoadJavaFXApplicationShould {

	public static final Logger log = LogManager.getLogger(LoadJavaFXApplicationShould.class);

	@Rule
	public TemporaryFolder fakeSystem = new TemporaryFolder();

//	private LoadJavaFXApplication loadJavaFXApplication;

	private static String classpath = "desktopbuddy.fx.DesktopBuddy";
	private static String runtimePath;
	private static String tempFolder;
	private static String downloadFileName = "updated_runtime.zip";
	private static File newFXApplicationJar;
	private static File currentFXApplicationJar;
	private static File lastWorkingFXApplicationJar;

	@Before
	public void setup() throws IOException {

		runtimePath = fakeSystem.newFolder("runtimePath").getAbsolutePath() + File.separator;
		tempFolder = fakeSystem.newFolder("tempFolder").getAbsolutePath() + File.separator;

//		loadJavaFXApplication = new LoadJavaFXApplication(classpath, runtimePath, tempFolder);
//		Mockito.spy(loadJavaFXApplication);

//		assertEquals(runtimePath, loadJavaFXApplication.runtimePath());
//		assertEquals(tempFolder, loadJavaFXApplication.tempFolder());

	}

	@Test
	public void runtime_file_exists_and_we_got_a_new_version_file() throws Exception {

		// el jar current tiene q estar y el new

		newFXApplicationJar = fakeSystem.newFile("fxapplication.jar.new");
		currentFXApplicationJar = fakeSystem.newFile("fxapplication.jar");
		lastWorkingFXApplicationJar = fakeSystem.newFile("fxapplication.jar.old");

//		loadJavaFXApplication.loadJavaFXApplicationJar(newFXApplicationJar, currentFXApplicationJar, lastWorkingFXApplicationJar, downloadFileName);

		assertEquals(newFXApplicationJar, currentFXApplicationJar);

	}

	@Test
	public void runtime_file_exists_and_we_got_a_corrupted_new_version_file() {
		// está el runtime.jar en el sitio, añadimos un runtime.jar.new corrupto
	}

	@Test
	public void runtime_file_exists_and_we_got_a_new_version_file_but_we_cant_move_the_files_to_make_them_active() {
		// está el runtime.jar en el sitio, añadimos un runtime.jar.new pero runtime.jar no puede sobreescribirse por permisos - no se ha podido probar en mac
	}

	@Test
	public void no_runtime_file_is_present_with_internet_connection_available() {
		// borramos todos los .jar e intentamos descargar un link q no existe
	}

	@Test
	public void no_runtime_file_is_present_and_no_internet_connection() {
		// en el caso de no tener nada y no tener conexión a internet
	}

	@Test
	public void only_old_runtime_file_is_present() {
		// si solo tenemos el runtime.jar.old file
	}

}
